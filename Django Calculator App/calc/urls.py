from django.urls import path
from calc import views

urlpatterns = [
    path('v2/', views.index, name='index'),
]
