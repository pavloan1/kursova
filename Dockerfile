FROM python:3.8

WORKDIR /app

COPY requirements.txt .

RUN python -m pip install --upgrade pip setuptools \
    && python -m venv /venv \
    && . /venv/bin/activate \
    && pip install --no-cache-dir -r requirements.txt django

COPY . .

EXPOSE 8050

CMD ["/venv/bin/python", "./Django Calculator App/manage.py", "runserver", "0.0.0.0:8050"]


